# Studio

Extract colors from an image and other color utilities.

## Install
Install with [composer](https://getcomposer.org/):

```
composer require americanart/studio
```

## Dependencies
- requires PHP 7.3 or greater.

## Functionality

- Find the english language name of a (HEX) color. 
- Convert colors between RGB, HSV, XYZ, and Lab color spaces.
- Compare colors.
- Extract colors as a palette from an image.

## Usage

### Get the RGB values of a color

```php
<?php

$color = \AmericanArt\Studio\ColorFactory::new()->createFromHex('#4682b4');
$rgb = $color->getRgb();
echo "Red: {$rgb['R']}, Green: {$rgb['G']}, Blue: {$rgb['B']}";
// output: "Red: 70, Green: 130, Blue: 180"
```

### Get the closest CSS4 color to a given color

```php
<?php

$picker = new AmericanArt\Studio\ColorPicker();
$color = \AmericanArt\Studio\ColorFactory::new()->createFromRgb(131, 246, 0);
$closestColor = $picker->getClosestColor($color);
echo $closestColor->getName();
// output: "lawngreen"
```

### Get the 5 most common colors from an image

```php
<?php

$picker = new AmericanArt\Studio\ColorPicker();
$url = 'https://s3.amazonaws.com/assets.saam.media/files/styles/x_large/s3/files/images/1991/SAAM-1991.61_2.jpg';
$colors = $picker->getColorsFromFile($url, 5);
foreach($colors as $color) {
    echo $color->getHex() . ' ';
}
// output: #dd290e #7d0000 #01168b #1563eb #3fb666
```
