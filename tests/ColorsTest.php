<?php

namespace AmericanArt\Studio\Tests;

use PHPUnit\Framework\TestCase;
use AmericanArt\Studio\ColorPicker;

/**
 * Class ColorsTest
 *
 * @package Handy\Tests
 */
class ColorsTest extends TestCase
{

    /**
     * @test
     */
    public function getColorNameTest()
    {
        $picker = new ColorPicker();
        // An example hex color.
        $hex = '#83F600';
        // Finds the closest HEX color in the current palette.
        $closest = $picker->getClosestFromHex($hex);
        // Finds the name "lawngreen".
        $name = $picker::getColorName($closest);
        $this->assertSame('lawngreen', $name);
    }
}
