<?php

namespace AmericanArt\Studio;

class ColorConverter
{
    /**
     * @param int $integer
     * @param bool $withHash
     * @return string
     */
    public static function fromIntToHex(int $integer, $withHash = TRUE): string
    {
        return mb_strtolower(($withHash ? '#' : '').sprintf('%06X', $integer));
    }

    /**
     * @param string $hexColor
     *
     * @return int
     */
    public static function fromHexToInt(string $hexColor): int
    {
        return hexdec(ltrim($hexColor, '#'));
    }

    /**
     * @param array $hexColors
     *   An array of hex colors
     *
     * @return array
     */
    public static function hexColorsToInt(array $hexColors): array
    {
        return array_map('self::fromHexToInt', $hexColors);
    }

    /**
     * @param int $integer
     *
     * @return array
     */
    public static function fromIntToRgb(int $integer): array
    {
        return [
            'R' => $integer >> 16 & 0xFF,
            'G' => $integer >> 8 & 0xFF,
            'B' => $integer & 0xFF,
        ];
    }

    /**
     * @param array $components
     *
     * @return int
     */
    public static function fromRgbToInt(array $components)
    {
        return ($components['R'] * 65536) + ($components['G'] * 256) + ($components['B']);
    }

    /**
     * Get distance from the given colors.
     * @see: https://en.wikipedia.org/wiki/Color_difference#CIEDE2000
     *
     * @param Color $first
     * @param Color $second
     * @return float
     */
    public static function getDistanceFromColors(Color $first, Color $second): float
    {
        $lab1 = self::fromColorToLab($first);
        $lab2 = self::fromColorToLab($second);

        $C1 = sqrt(($lab1['a'] ** 2) + ($lab1['b'] ** 2));
        $C2 = sqrt(($lab2['a'] ** 2) + ($lab2['b'] ** 2));
        $Cb = ($C1 + $C2) / 2;

        $G = .5 * (1 - sqrt(($Cb ** 7) / (($Cb ** 7) + (25 ** 7))));

        $a1p = (1 + $G) * $lab1['a'];
        $a2p = (1 + $G) * $lab2['a'];

        $C1p = sqrt(($a1p ** 2) + ($lab1['b'] ** 2));
        $C2p = sqrt(($a2p ** 2) + ($lab2['b'] ** 2));

        $h1p = $a1p === 0 && $lab1['b'] === 0 ? 0 : atan2($lab1['b'], $a1p);
        $h2p = $a2p === 0 && $lab2['b'] === 0 ? 0 : atan2($lab2['b'], $a2p);

        $LpDelta = $lab2['L'] - $lab1['L'];
        $CpDelta = $C2p - $C1p;

        if ($C1p * $C2p === 0) {
            $hpDelta = 0;
        } elseif (abs($h2p - $h1p) <= 180) {
            $hpDelta = $h2p - $h1p;
        } elseif ($h2p - $h1p > 180) {
            $hpDelta = $h2p - $h1p - 360;
        } else {
            $hpDelta = $h2p - $h1p + 360;
        }

        $HpDelta = 2 * sqrt($C1p * $C2p) * sin($hpDelta / 2);

        $Lbp = ($lab1['L'] + $lab2['L']) / 2;
        $Cbp = ($C1p + $C2p) / 2;

        if ($C1p * $C2p === 0) {
            $hbp = $h1p + $h2p;
        } elseif (abs($h1p - $h2p) <= 180) {
            $hbp = ($h1p + $h2p) / 2;
        } elseif ($h1p + $h2p < 360) {
            $hbp = ($h1p + $h2p + 360) / 2;
        } else {
            $hbp = ($h1p + $h2p - 360) / 2;
        }

        $T = 1 - .17 * cos($hbp - 30) + .24 * cos(2 * $hbp) + .32 * cos(3 * $hbp + 6) - .2 * cos(4 * $hbp - 63);

        $sigmaDelta = 30 * exp(-(($hbp - 275) / 25) ** 2);

        $Rc = 2 * sqrt(($Cbp ** 7) / (($Cbp ** 7) + (25 ** 7)));

        $Sl = 1 + ((.015 * (($Lbp - 50) ** 2)) / sqrt(20 + (($Lbp - 50) ** 2)));
        $Sc = 1 + .045 * $Cbp;
        $Sh = 1 + .015 * $Cbp * $T;

        $Rt = -sin(2 * $sigmaDelta) * $Rc;

        return sqrt(
            (($LpDelta / $Sl) ** 2) +
            (($CpDelta / $Sc) ** 2) +
            (($HpDelta / $Sh) ** 2) +
            $Rt * ($CpDelta / $Sc) * ($HpDelta / $Sh)
        );

    }

    /**
     * Get color CIE-Lab values
     *
     * @param Color $color
     * @return array
     */
    public static function fromColorToLab(Color $color): array
    {
        $xyz = self::fromColorToXyz($color);
        //Observer = 2*, Illuminant=D65
        $xyz['X'] /= 95.047;
        $xyz['Y'] /= 100;
        $xyz['Z'] /= 108.883;
        $xyz = array_map(
            static function ($item) {
                if ($item > 0.008856) {
                    return $item ** (1 / 3);
                }
                return (7.787 * $item) + (16 / 116);
            },
            $xyz
        );
        return [
            'L' => (116 * $xyz['Y']) - 16,
            'a' => 500 * ($xyz['X'] - $xyz['Y']),
            'b' => 200 * ($xyz['Y'] - $xyz['Z']),
        ];
    }

    public static function fromIntToLab(int $integer): array
    {
        $color = ColorFactory::new()->createFromInt($integer);
        return self::fromColorToLab($color);
    }

        /**
     * Get color in XYZ format
     *
     * @param Color $color
     * @return array
     */
    public static function fromColorToXyz(Color $color): array
    {
        $rgb = $color->getRgb();
        // Normalize RGB values to 1
        $rgb = array_map(
            static function ($item) {
                return $item / 255;
            },
            $rgb
        );
        $rgb = array_map(
            static function ($item) {
                if ($item > 0.04045) {
                    $item = (($item + 0.055) / 1.055) ** 2.4;
                } else {
                    $item /= 12.92;
                }
                return ($item * 100);
            },
            $rgb
        );
        //Observer. = 2°, Illuminant = D65
        return [
            'X' => ($rgb['R'] * 0.4124) + ($rgb['G'] * 0.3576) + ($rgb['B'] * 0.1805),
            'Y' => ($rgb['R'] * 0.2126) + ($rgb['G'] * 0.7152) + ($rgb['B'] * 0.0722),
            'Z' => ($rgb['R'] * 0.0193) + ($rgb['G'] * 0.1192) + ($rgb['B'] * 0.9505)
        ];
    }

}
