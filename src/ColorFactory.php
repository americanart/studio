<?php

namespace AmericanArt\Studio;

use Exception;

class ColorFactory
{
    public static function new(): self
    {
        return new self();
    }

    public function createFromHex(string $hex): Color
    {
        $hex = self::sanitizeHex($hex);
        $integer = ColorConverter::fromHexToInt($hex);
        return new Color($integer);
    }

    public function createFromInt(int $integer): Color
    {
        return new Color($integer);
    }

    public function createFromRgb(int $red, int $green, int $blue): Color
    {
        $integer = ColorConverter::fromRgbToInt(['R' => $red, 'G' => $green, 'B' => $blue]);
        return new Color($integer);
    }

    /**
     * Checks the HEX string for correct formatting and converts short format to long
     * @param string $hex
     * @return string
     * @throws Exception
     */
    private static function sanitizeHex(string $hex): string
    {
        // Strip # sign if it is present
        $color = str_replace("#", "", $hex);
        // Validate hex string
        if (!preg_match('/^[a-fA-F0-9]+$/', $color)) {
            throw new \RuntimeException("HEX color does not match format");
        }
        // Make sure it's 6 digits
        if (strlen($color) === 3) {
            $color = $color[0] . $color[0] . $color[1] . $color[1] . $color[2] . $color[2];
        } elseif (strlen($color) !== 6) {
            throw new \RuntimeException("HEX color needs to be 6 or 3 digits long");
        }

        return $color;
    }

}
