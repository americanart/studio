<?php

namespace AmericanArt\Studio\ColorExtractor;

use AmericanArt\Studio\ColorConverter;
use AmericanArt\Studio\ColorFactory;
use SplFixedArray;

class ColorExtractor
{
    /** @var Palette */
    protected $palette;

    /** @var SplFixedArray */
    protected $sortedColors;

    /**
     * @param Palette $palette
     */
    public function __construct(Palette $palette)
    {
        $this->palette = $palette;
    }

    /**
     * @param int $colorCount
     *
     * @return array
     */
    public function extract($colorCount = 1): array
    {
        if (!$this->isInitialized()) {
            $this->initialize();
        }

        return self::mergeColors($this->sortedColors, $colorCount, 100 / $colorCount);
    }

    /**
     * @return bool
     */
    protected function isInitialized(): bool
    {
        return $this->sortedColors !== null;
    }

    protected function initialize(): void
    {
        $queue = new \SplPriorityQueue();
        $this->sortedColors = new SplFixedArray(count($this->palette));

        $i = 0;
        foreach ($this->palette as $integer => $count) {
            $labColor = ColorConverter::fromIntToLab($integer);
            $queue->insert(
                $integer,
                (sqrt($labColor['a'] * $labColor['a'] + $labColor['b'] * $labColor['b']) ?: 1) *
                (1 - $labColor['L'] / 200) *
                sqrt($count)
            );
            ++$i;
        }

        $i = 0;
        while ($queue->valid()) {
            $this->sortedColors[$i] = $queue->current();
            $queue->next();
            ++$i;
        }
    }

    /**
     * @param SplFixedArray $colors
     * @param int $limit
     * @param int $maxDelta
     *
     * @return array
     */
    protected static function mergeColors(SplFixedArray $colors, int $limit, int $maxDelta): array
    {
        $limit = min(count($colors), $limit);
        if ($limit === 1) {
            return [$colors[0]];
        }
        $cache = new SplFixedArray($limit - 1);
        $mergedColors = [];

        foreach ($colors as $integer) {
            $hasColorBeenMerged = false;
            foreach ($mergedColors as $i => $mergedColor) {
                $color = ColorFactory::new()->createFromInt($integer);
                $cachedColor = ColorFactory::new()->createFromInt($cache[$i]);
                if (ColorConverter::getDistanceFromColors($color, $cachedColor) < $maxDelta) {
                    $hasColorBeenMerged = true;
                    break;
                }
                unset($color, $cachedColor);
            }
            if ($hasColorBeenMerged) {
                continue;
            }
            $mergedColorCount = count($mergedColors);
            $mergedColors[] = $integer;
            if ($mergedColorCount + 1 === $limit) {
                break;
            }
            $cache[$mergedColorCount] = $integer;
        }

        return $mergedColors;
    }

}
