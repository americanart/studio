<?php

namespace AmericanArt\Studio;

/**
 * Color object.
 */
class Color
{
    /**
     * @var int
     */
    private $integer;

    /**
     * @var string
     */
    private $hex;

    /**
     * @var array
     */
    private $rgb;

    /**
     * @var string
     */
    private $name = '';

    public function __construct(int $integer)
    {
        $this->integer = $integer;
        $this->hex = ColorConverter::fromIntToHex($integer);
        $this->rgb = ColorConverter::fromIntToRgb($integer);
        $namedColors = ColorNames::CSS4;
        if (isset($namedColors[$this->hex], $namedColors[$this->hex]['name'])) {
            $this->name = $namedColors[$this->hex]['name'];
        }
    }

    public function getInt(): int
    {
        return $this->integer;
    }

    public function getHex(): string
    {
        return $this->hex;
    }

    public function getRgb(): array
    {
        return $this->rgb;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function __toString(): string
    {
        return $this->toString();
    }

    public function toString(): string
    {
        return $this->hex;
    }

    public function toArray(): array
    {
        return [
            'int' => $this->getInt(),
            'hex' => $this->getHex(),
            'rgb' => $this->getRgb(),
            'name' => $this->getName(),
        ];
    }

}
