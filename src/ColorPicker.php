<?php

namespace AmericanArt\Studio;

use AmericanArt\Studio\ColorExtractor\ColorExtractor;
use AmericanArt\Studio\ColorExtractor\Palette;

/**
 * Class ColorPicker
 */
class ColorPicker
{

    /**
     * Extract colors from an image file.
     *
     * @param string $filename
     * @param int $num_colors
     * @param null $crop_background_color
     * @return Color[]
     */
    public function getColorsFromFile(string $filename, $num_colors = 4, $crop_background_color = NULL): array
    {
        $image = $this->getImage($filename);
        if (!is_null($crop_background_color)) {
            $image = imagecropauto($image, IMG_CROP_THRESHOLD, 10, $crop_background_color);
        }
        $palette = Palette::fromGD($image);
        imagedestroy($image);
        $extractor = new ColorExtractor($palette);
        $extractedColors = $extractor->extract($num_colors);
        $colors = [];
        foreach ($extractedColors as $integer) {
            $colors[] = ColorFactory::new()->createFromInt($integer);
        }
        return $colors;
    }

    /**
     * Get a GD Image from file.
     *
     * @param string $filename
     *
     * @return resource|FALSE
     */
    private function getImage(string $filename)
    {
        return imagecreatefromstring(file_get_contents($filename));
    }

    /**
     * Find the the closest matching CSS4 color.
     *
     * @param Color $color
     *   A color object
     *
     * @return Color
     *   The closest color
     */
    public function getClosestColor(Color $color): Color
    {
        // Convert colors to an array of Int color values.
        $palette = array_keys(ColorNames::CSS4);
        $searchablePalette = ColorConverter::hexColorsToInt($palette);
        // Get the index in the search palette of the closest matching color.
        $matchIndex = $this->getClosestMatch($color, $searchablePalette);
        return ColorFactory::new()->createFromInt($searchablePalette[$matchIndex]);
    }

    /**
     * Get the closest matching the given color from the an array of Colors
     *
     * @param Color $color
     * @param Color[]|int[] $colors
     *   array of integers or ColorList objects
     *
     * @return mixed the array key of the matched color
     */
    public function getClosestMatch(Color $color, array $colors)
    {
        $matchDist = 10000;
        $matchKey = null;
        foreach ($colors as $key => $item) {
            if (false === ($item instanceof Color)) {
                $item = ColorFactory::new()->createFromInt($item);
            }
            $dist = ColorConverter::getDistanceFromColors($color, $item);
            if ($dist < $matchDist) {
                $matchDist = $dist;
                $matchKey = $key;
            }
        }
        return $matchKey;
    }

}
